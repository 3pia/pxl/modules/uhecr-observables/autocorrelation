//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//          http://pxl.sourceforge.net      -
// Copyright (C) 2009-2010 Martin Erdmann        -
//               RWTH Aachen, Germany       -
// Contact: pxl-users@lists.sourceforge.net -
//-------------------------------------------

#include "pxl/core/macros.hh"
#include "pxl/core/PluginManager.hh"
#include "pxl/modules/Module.hh"
#include "pxl/modules/ModuleFactory.hh"

#include "pxl/core/BasicNVector.hh"
#include "pxl/core/BasicContainer.hh"
#include "pxl/astro.hh"
#include "pxl/astro/UHECR.hh"

#include <iostream>

class AutoCorrelation: public pxl::Module
{
private:
	int64_t _Nbins;
	double _maxAngle;
	double _energyThreshold;
	bool _useUserRecord;
	std::string _name;

public:

	// initialize the super class Module as well
	AutoCorrelation(): _Nbins(0), _maxAngle(0), _energyThreshold(0), _useUserRecord(false),
		Module()
	{
	}


	void initialize()
	{
		addOption("maxAngle", "MAximum angle [deg]", double(60));
		addOption("numberOFBins", "Number of angular bins.", int64_t(60));
		addOption("energyThreshold", "Energy threshold [EeV].", double(60));
		addOption("name", "Name of the correlation.", "AutoCorrelationDistribution");
		addOption("useUserRecord", "Store the result in the user record.", true);
		addSink("input", "Input");
		addSource("output", "Output");
	}


	~AutoCorrelation()
	{
	}


	// every Module needs a unique type
	static const std::string &getStaticType()
	{
		static std::string type("AutoCorrelation");
		return type;
	}


	// static and dynamic methods are needed
	const std::string &getType() const
	{
		return getStaticType();
	}


	bool isRunnable() const
	{
		// this module does not proide events, so return false
		return false;
	}


	void beginJob() throw (std::runtime_error)
	{
		// copy option to local variables to avoid map search due to
		// performance
		getOption("maxAngle", _maxAngle);
		// _maxAngle in rad
		_maxAngle*=M_PI/180.;
		getOption("numberOFBins", _Nbins);
		getOption("energyThreshold", _energyThreshold);
		getOption("useUserRecord", _useUserRecord);
		getOption("name", _name);
	}


	bool analyse(pxl::Sink *sink) throw (std::runtime_error)
	{
		getSource("output")->setTargets(getSink("input")->get());
		pxl::BasicContainer* basiccontainer = dynamic_cast<pxl::BasicContainer*> (getSink("input")->get());

		pxl::BasicNVector* _autoCorrelationDistribution = new pxl::BasicNVector(_Nbins);

		std::vector<pxl::UHECR*> uhecrs;
		basiccontainer->getObjectsOfType<pxl::UHECR>(uhecrs);

		for (std::vector<pxl::UHECR*>::const_iterator iter1 = uhecrs.begin(); iter1 != uhecrs.end(); ++iter1)
		{
			if ((*iter1)->getEnergy() < _energyThreshold)
				continue;
			for (std::vector<pxl::UHECR*>::const_iterator iter2 = iter1+1; iter2 != uhecrs.end(); ++iter2)
			{
				if ((*iter2)->getEnergy() < _energyThreshold)
					continue;
				double alpha = (*iter1)->angularDistanceTo(*iter2);
				if (alpha < _maxAngle)
				{
					int64_t idx = (alpha/_maxAngle)*_Nbins;
					(*_autoCorrelationDistribution)(idx)+=1;
				}
			}
		}

		//integrate distribution
		for(int i=1; i<_Nbins;i++)
		{
			(*_autoCorrelationDistribution)(i) += (*_autoCorrelationDistribution)(i-1);
		}

		
		_autoCorrelationDistribution->setName(_name);
		if (_useUserRecord)
		{
			basiccontainer->setUserRecord("AutoCorrelationDistribution",_autoCorrelationDistribution);
			delete _autoCorrelationDistribution;
		}
		else
		{
			basiccontainer->insertObject(_autoCorrelationDistribution);
		}

		return getSource("output")->processTargets();
	}


	void destroy() throw (std::runtime_error)
	{
		// only we know how to delete this module
		delete this;
	}
};

PXL_MODULE_INIT(AutoCorrelation)
PXL_PLUGIN_INIT

