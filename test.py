import sys
if sys.version_info[0] == 2 and sys.version_info[1] < 7:
  import unittest2 as unittest
else:
  import unittest
from pxl import core, astro, modules

from math import *

core.PluginManager.instance().loadPluginsFromDirectory(".")


class TestAutoCorrelation(unittest.TestCase):
  def setUp(self):
    self.m = modules.ModuleFactory.instance().createModule("AutoCorrelation")
    self.m.initialize()
    self.m.beginJob()

  def testEmptyContainer(self):
    bc = core.BasicContainer()
    self.m.getSink("input").set(bc)
    self.m.analyse(self.m.getSink("input"))
    ac = bc.getUserRecord("AutoCorrelationDistribution")

    for i in range(ac.getSize()):
      self.assertTrue(abs(ac[i]) < 0.1 )


  def testOneUHECR(self):
    bc = core.BasicContainer()

    u = astro.UHECR()
    bc.insertObject(u)

    self.m.getSink("input").set(bc)
    self.m.analyse(self.m.getSink("input"))
    ac = bc.getUserRecord("AutoCorrelationDistribution")

    for i in range(ac.getSize()):
      self.assertTrue(abs(ac[i]) < 0.1 )


  def testCloseUHECREnergyThreshold(self):
    bc = core.BasicContainer()

    u1 = astro.UHECR()
    u1.setEnergy(70)
    u2 = astro.UHECR()
    u2.setEnergy(50)
    bc.insertObject(u1)
    bc.insertObject(u2)

    self.m.getSink("input").set(bc)
    self.m.analyse(self.m.getSink("input"))
    ac = bc.getUserRecord("AutoCorrelationDistribution")

    for i in range(ac.getSize()):
      self.assertTrue(abs(ac[i]) < 0.1 )


  def testCloseUHECR(self):
    bc = core.BasicContainer()

    u1 = astro.UHECR()
    u1.setEnergy(70)
    u2 = astro.UHECR()
    u2.setEnergy(70)
    bc.insertObject(u1)
    bc.insertObject(u2)

    self.m.getSink("input").set(bc)
    self.m.analyse(self.m.getSink("input"))
    ac = bc.getUserRecord("AutoCorrelationDistribution")

    for i in range(ac.getSize()):
      self.assertTrue(abs(ac[i] - 1) < 0.1 )


  def testSeperatedUHECR(self):
    bc = core.BasicContainer()
    u1 = astro.UHECR()
    u1.setEnergy(70)
    u2 = astro.UHECR()
    u2.setEnergy(70)
    u2.setGalacticCoordinates(0,9.5/180*pi)
    bc.insertObject(u1)
    bc.insertObject(u2)

    self.m.getSink("input").set(bc)
    self.m.analyse(self.m.getSink("input"))
    ac = bc.getUserRecord("AutoCorrelationDistribution")
    for i in range(ac.getSize())[:9]:
      self.assertTrue(abs(ac[i] ) < 0.1 )
    for i in range(ac.getSize())[9:]:
      self.assertTrue(abs(ac[i] - 1) < 0.1 )

    bc = core.BasicContainer()
    u1 = astro.UHECR()
    u1.setEnergy(70)
    u2 = astro.UHECR()
    u2.setEnergy(70)
    u2.setGalacticCoordinates(0,10.1/180*pi)
    bc.insertObject(u1)
    bc.insertObject(u2)

    self.m.getSink("input").set(bc)
    self.m.analyse(self.m.getSink("input"))
    ac = bc.getUserRecord("AutoCorrelationDistribution")
    for i in range(ac.getSize())[:10]:
      self.assertTrue(abs(ac[i] ) < 0.1 )
    for i in range(ac.getSize())[10:]:
      self.assertTrue(abs(ac[i] - 1) < 0.1 )


  def testSeperatedUHECR_border(self):
    bc = core.BasicContainer()
    u1 = astro.UHECR()
    u1.setEnergy(70)
    u2 = astro.UHECR()
    u2.setEnergy(70)
    u2.setGalacticCoordinates(0,60.1/180*pi)
    bc.insertObject(u1)
    bc.insertObject(u2)

    self.m.getSink("input").set(bc)
    self.m.analyse(self.m.getSink("input"))
    ac = bc.getUserRecord("AutoCorrelationDistribution")
    for i in range(ac.getSize()):
      self.assertTrue(abs(ac[i] ) < 0.1 )



if __name__ == '__main__':
    unittest.main()


